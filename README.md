# devops-lab App API Proxy

NGINX proxy for our app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen (default: `8000`)
* `APP_HOST` - Host name of the app to foward requests to (default: `app`)
* `APP_PORT` - Port of the app to foward requests to (default: `9000`)

devops-lab@2022
